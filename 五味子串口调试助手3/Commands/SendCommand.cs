﻿

using System.Windows.Input;
namespace 五味子串口调试助手3
{
    public class SendCommand : ICommand
    {
        public MainWindowModel ViewModel { get; set; }

        public SendCommand(MainWindowModel ViewModel)
        {
            this.ViewModel = ViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return ViewModel.commIsOpen;
        }

        public void Execute(object parameter)
        {
            if (ViewModel.isSendHex)
            {
                ViewModel.sendText = tools.orderHexString(ViewModel.sendText);
                ViewModel.send(new OneSerialDataClass(ViewModel.sendText + ViewModel.sendCheckText, true));
            }
            else
            {
                ViewModel.send(new OneSerialDataClass(ViewModel.sendText, false));
            }
        }

        public event System.EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using 五味子串口调试助手3.Properties;

namespace 五味子串口调试助手3
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowModel ViewModel { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MainWindowModel();
            ViewModel.mainWindow = this;
            ViewModel.commPort = "COM1";
            ViewModel.bandRate = 4800;
            ViewModel.dataBits = 8;
            ViewModel.stopBits = 1;
            ViewModel.verfyBits = "None";

            ViewModel.clcInterval = Settings.Default.clcInterval;
            ViewModel.isDisplayHex = true;
            ViewModel.isSendHex = true;

            string[] clcSendArr = Settings.Default.clcSendText.Split(new char[1]{'|'},StringSplitOptions.RemoveEmptyEntries);
            foreach(var v in clcSendArr)
            {
                ViewModel.list_clcSend.Add(new clcSendDataClass() { sendText = v });
            }

            string[] conditionSendArr = Settings.Default.conditionSendText.Split(new char[1] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var v in conditionSendArr)
            {
                string[] s = v.Split(new char[1] { ';' });
                if(s.Length == 2)
                {
                    ViewModel.list_conditionSend.Add(new conditionSendDataClass() { reciveText = s[0], sendText = s[1] });
                }
            }

            display_dataGridCol(new object(), new System.Windows.RoutedEventArgs());

            this.DataContext = ViewModel;
        }

        private void ComboBox_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            (sender as System.Windows.Controls.ComboBox).ItemsSource = ViewModel.commPortArr;
            e.Handled = true;
        }

        private void display_dataGridCol(object sender, System.Windows.RoutedEventArgs e)
        {

            foreach (DataGridColumn current in this.dataGrid_recive.Columns)
            {
                string headerString = current.Header.ToString().ToLower();
                if (headerString.Contains("hex"))
                {
                    current.Visibility = (ViewModel.isDisplayHex ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed);
                }
                else if (headerString.Contains("ascii"))
                {
                    current.Visibility = (ViewModel.isDisplayAscii ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed);
                }
            }
        }

        private void delRow(object sender, System.Windows.RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button.Tag is clcSendDataClass)
            {
                clcSendDataClass data = button.Tag as clcSendDataClass;
                ViewModel.list_clcSend.Remove(data);
            }
            else if (button.Tag is conditionSendDataClass)
            {
                conditionSendDataClass data = button.Tag as conditionSendDataClass;
                ViewModel.list_conditionSend.Remove(data);
            }
        }

        private void WindowViewBase_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ViewModel.stopThread = true;

            Settings.Default.sendText = ViewModel.sendText;

            Settings.Default.clcInterval = ViewModel.clcInterval;

            Settings.Default.clcSendText = "";
            foreach(var v in ViewModel.list_clcSend)
            {
                Settings.Default.clcSendText += v.sendText + "|";
            }

            Settings.Default.conditionSendText = "";
            foreach(var v in ViewModel.list_conditionSend)
            {
                Settings.Default.conditionSendText += v.reciveText + ";" + v.sendText + "|";
            }
            Settings.Default.Save();
        }

        private void GroupBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ViewModel.sendBytesNum = 0;
            ViewModel.sendMessageNum = 0;
            ViewModel.reciveBytesNum = 0;
            ViewModel.reciveMessageNum = 0;
        }

        private void btn_clearRecive_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.list_allData.Clear();
        }

        private void btn_copy_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string text = string.Format("{0,-5}  {1}  {2}  {3}  {4}", new object[]
				{
					"序号",
					"方向",
					"时间",
					"HEX字符串",
					"ASCII字符串"
				}); ;
            int num = 1;

            foreach (var v in ViewModel.list_allData)
            {
                text += string.Format("{0,-5}  {1}  {2}  {3}  {4}", new object[]
				{
					num++,
					v.inOrOut,
					v.buildTime,
					v.hexString,
					ViewModel.isDisplayAscii ? v.asciiString : ""
				});
            }
            try
            {
                System.Windows.Clipboard.SetText(text);
            }
            catch (Exception)
            {
            }
        }

        private void btn_stopRecive_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.isPauseDisplay = !ViewModel.isPauseDisplay;
            (sender as Button).Content = ViewModel.isPauseDisplay ? "继续显示" : "暂停显示";
        }

        private void reflushCheckText(object sender, System.Windows.RoutedEventArgs e)
        {
            if (ViewModel != null && ViewModel.checkMethod != null && ViewModel.sendText != null && ViewModel.sendText != "")
            {
                ViewModel.sendCheckText = ViewModel.checkMethod.checkData(ViewModel.sendText);
            }
        }
    }
}

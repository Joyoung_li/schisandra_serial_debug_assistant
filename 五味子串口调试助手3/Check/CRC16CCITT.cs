﻿using System;

namespace 五味子串口调试助手3
{
    public class CRC16CCITT : checkBase
    {
        public override string checkData(string s)
        {
            byte[] array = tools.hexStringToByte(s);
            return tools.bytesToHexString(BitConverter.GetBytes(this._CRC16CCITT(array, array.Length)));
        }

        private ushort _CRC16CCITT(byte[] pszBuf, int unLength)
        {
            ushort num = 65535;
            for (int i = 0; i < unLength; i++)
            {
                ushort num2 = (ushort)(pszBuf[i] << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((short)(num ^ num2) < 0)
                    {
                        num = (ushort)((int)num << 1 ^ 4129);
                    }
                    else
                    {
                        num = (ushort)(num << 1);
                    }
                    num2 = (ushort)(num2 << 1);
                }
            }
            return num;
        }

        public override string ToString()
        {
            return "CRC16CCITT";
        }
    }

}

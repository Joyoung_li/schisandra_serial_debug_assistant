﻿using System;

namespace 五味子串口调试助手3
{
    public class CRC16RTU : checkBase
    {
        public override string checkData(string s)
        {
            byte[] array = tools.hexStringToByte(s);
            return tools.bytesToHexString(BitConverter.GetBytes(this._CRC16RTU(array, array.Length)));
        }

        private ushort _CRC16RTU(byte[] pszBuf, int unLength)
        {
            ushort num = 65535;
            for (int i = 0; i < unLength; i++)
            {
                num = Convert.ToUInt16((int)(num ^ (ushort)pszBuf[i]));
                for (int j = 0; j < 8; j++)
                {
                    if ((num & 1) == 1)
                    {
                        num = (ushort)(num >> 1);
                        num ^= 40961;
                    }
                    else
                    {
                        num = (ushort)(num >> 1);
                    }
                }
            }
            return num;
        }

        public override string ToString()
        {
            return "CRC16RTU";
        }
    }


}

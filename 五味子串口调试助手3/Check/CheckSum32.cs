﻿using System;

namespace 五味子串口调试助手3
{
    public class CheckSum32 : checkBase
    {
        public override string checkData(string s)
        {
            byte[] array = tools.hexStringToByte(s);
            return tools.bytesToHexString(BitConverter.GetBytes(this._CheckSum32(array, array.Length)));
        }

        private uint _CheckSum32(byte[] buf, int len)
        {
            uint num = 0u;
            for (int i = 0; i < len; i++)
            {
                num += (uint)buf[i];
            }
            return num;
        }

        public override string ToString()
        {
            return "校验和-32";
        }
    }
}

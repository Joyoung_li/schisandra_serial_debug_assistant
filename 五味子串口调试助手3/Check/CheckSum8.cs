﻿
namespace 五味子串口调试助手3
{
    public class CheckSum8 : checkBase
    {
        public override string checkData(string s)
        {
            byte[] array = tools.hexStringToByte(s);
            return this._CheckSum8(array, array.Length).ToString("X2");
        }

        private byte _CheckSum8(byte[] buf, int len)
        {
            byte b = 0;
            for (int i = 0; i < len; i++)
            {
                b += buf[i];
            }
            return b;
        }

        public override string ToString()
        {
            return "校验和-8";
        }
    }
}

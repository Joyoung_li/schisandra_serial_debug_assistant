﻿using System;
using System.Text;

namespace 五味子串口调试助手3
{
    public enum InOrOut
    {
        IN,
        OUT,
    }
    public class OneSerialDataClass
    {
        public InOrOut inOrOut { get; set; }
        public string hexString { get; set; }
        public string asciiString { get; set; }
        public string buildTime { get; set; }


        public byte[] bytes { get; set; }

        public OneSerialDataClass(string sendText, bool isSendHex = true)
        {
            if (isSendHex)
            {
                hexString = sendText.Trim().Replace(" ","");
                asciiString = tools.hexStr2Str(hexString);
            }
            else
            {
                asciiString = sendText;
                hexString = tools.str2HexStr(asciiString);
            }
            hexString = tools.orderHexString(hexString);

            bytes = tools.hexStringToByte(hexString);

            buildTime = DateTime.Now.ToString("HH:mm:ss fff");
            inOrOut =  InOrOut.OUT;
        }

        public OneSerialDataClass(byte[] reciveArray, int reciveLength)
        {
            byte[] temp = new byte[reciveLength];
            Array.Copy(reciveArray, temp, reciveLength);
            asciiString = Encoding.Default.GetString(temp);
            hexString = tools.orderHexString(tools.bytesToHexString(temp));

            bytes = tools.hexStringToByte(hexString);

            buildTime = DateTime.Now.ToString("HH:mm:ss fff");
            inOrOut =  InOrOut.IN;
        }
    }
}

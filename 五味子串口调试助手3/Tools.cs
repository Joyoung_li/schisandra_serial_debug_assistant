﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace 五味子串口调试助手3
{
    // Token: 0x02000018 RID: 24
    internal class tools
    {
        // Token: 0x0600007A RID: 122 RVA: 0x00002C48 File Offset: 0x00000E48
        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T t = default(T);
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                Visual visual = (Visual)VisualTreeHelper.GetChild(parent, i);
                t = (visual as T);
                if (t == null)
                {
                    t = tools.GetVisualChild<T>(visual);
                }
                if (t != null)
                {
                    break;
                }
            }
            return t;
        }

        // Token: 0x0600007B RID: 123 RVA: 0x00002CA4 File Offset: 0x00000EA4
        public static T GetParentObject<T>(DependencyObject obj) where T : FrameworkElement
        {
            for (DependencyObject parent = VisualTreeHelper.GetParent(obj); parent != null; parent = VisualTreeHelper.GetParent(parent))
            {
                if (parent is T)
                {
                    return (T)((object)parent);
                }
            }
            return default(T);
        }

        // Token: 0x0600007C RID: 124 RVA: 0x00002CDC File Offset: 0x00000EDC
        public static Type[] GetChildTypes(Type parentType)
        {
            List<Type> list = new List<Type>();
            Assembly assembly = Assembly.GetAssembly(parentType);
            Type[] types = assembly.GetTypes();
            for (int i = 0; i < types.Length; i++)
            {
                Type type = types[i];
                if (type.BaseType == parentType)
                {
                    list.Add(type);
                }
            }
            return list.ToArray();
        }

        // Token: 0x0600007D RID: 125 RVA: 0x00002D30 File Offset: 0x00000F30
        public static string str2HexStr(string str)
        {
            char[] array = "0123456789ABCDEF".ToCharArray();
            StringBuilder stringBuilder = new StringBuilder("");
            char arg_1D_0 = str[0];
            byte[] bytes = Encoding.Default.GetBytes(str);
            for (int i = 0; i < bytes.Length; i++)
            {
                int num = (bytes[i] & 240) >> 4;
                stringBuilder.Append(array[num]);
                num = (int)(bytes[i] & 15);
                stringBuilder.Append(array[num]);
                stringBuilder.Append(' ');
            }
            return stringBuilder.ToString();
        }

        // Token: 0x0600007E RID: 126 RVA: 0x00002DB4 File Offset: 0x00000FB4
        public static string hexStr2Str(string hexStr)
        {
            string text = "0123456789ABCDEF";
            hexStr = hexStr.Replace(" ", "");
            char[] array = hexStr.ToCharArray();
            byte[] array2 = new byte[hexStr.Length / 2];
            for (int i = 0; i < array2.Length; i++)
            {
                int num = text.IndexOf(array[2 * i]) * 16;
                num += text.IndexOf(array[2 * i + 1]);
                array2[i] = (byte)(num & 255);
            }
            return Encoding.Default.GetString(array2);
        }

        // Token: 0x0600007F RID: 127 RVA: 0x00002E38 File Offset: 0x00001038
        public static byte[] hexStringToByte(string hex)
        {
            hex = hex.Replace(" ", "").ToUpper();
            int num = hex.Length / 2;
            byte[] array = new byte[num];
            char[] array2 = hex.ToCharArray();
            for (int i = 0; i < num; i++)
            {
                int num2 = i * 2;
                array[i] = (byte)(tools.toByte(array2[num2]) << 4 | tools.toByte(array2[num2 + 1]));
            }
            return array;
        }

        // Token: 0x06000080 RID: 128 RVA: 0x00002E9C File Offset: 0x0000109C
        private static int toByte(char c)
        {
            return (int)((byte)"0123456789ABCDEF".IndexOf(c));
        }

        // Token: 0x06000081 RID: 129 RVA: 0x00002EB8 File Offset: 0x000010B8
        public static string bytesToHexString(byte[] bArray)
        {
            StringBuilder stringBuilder = new StringBuilder(bArray.Length);
            for (int i = 0; i < bArray.Length; i++)
            {
                string text = bArray[i].ToString("X2");
                if (text.Length < 2)
                {
                    stringBuilder.Append(0);
                }
                stringBuilder.Append(text.ToUpper());
                stringBuilder.Append(' ');
            }
            return stringBuilder.ToString();
        }

        public static string orderHexString(string s)
        {
            if(s == null || s.Trim() == "")
            {
                return "";
            }
            string text = "";
            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];
                if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
                {
                    text += c.ToString().ToUpper();
                    if (text.Length % 3 == 2)
                    {
                        text += ' ';
                    }
                }
            }
            if (text.Trim().Length % 3 != 2)
            {
                text = text.Trim() + "0";
            }
            return text;
        }

        public static void removeDuplicate<T>(List<T> list)
        {
            HashSet<T> h = new HashSet<T>(list);
            list.Clear();
            list.AddRange(h);
        }
    }
}
